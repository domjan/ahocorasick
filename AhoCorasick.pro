#-------------------------------------------------
#
# Project created by QtCreator 2012-04-23T16:52:24
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = AhoCorasick
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    ahocorasick.cpp \
    machine.cpp \
    state.cpp

HEADERS += \
    ahocorasick.h \
    machine.h \
    state.h




