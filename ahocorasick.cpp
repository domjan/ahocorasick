#include "ahocorasick.h"

#include <algorithm>
#include <functional>

using namespace std;

/**
 * @brief AhoCorasick::AhoCorasick
 *          This constructor creates a new AhoChorasickMatcher.
 *          We should specify the strings we want to match.
 * @param tokens: strings which we want to match
 */
AhoCorasick::AhoCorasick(const std::vector<string> &tokens)
{
    for_each (tokens.begin(), tokens.end(),
              std::bind1st(std::mem_fun(&AhoCorasick::addString), this));
    machine.setErrorStates();
}


/**
 * @brief AhoCorasick::addString
 *          Adds a new string to the automaton.
 * @param token the token
 */
void AhoCorasick::addString(const string token)
{
    machine.setInitState();
    for(unsigned long i = 0; i<token.length(); i++)
    {
        machine.addNewState(token[i], (i==token.length()-1), token); //adds a new edge and state to the curent state
    }
}


/**
 * @brief AhoCorasick::matching
 * Matches the string with the tokens, added previously.
 * The method resets the machine and steps through the string.
 * @param str: the string
 */
void AhoCorasick::matching(const string &str)
{
    machine.setInitState();
    for(unsigned long i = 0; i<str.length(); i++)
    {
        machine.step(str, i);
    }
}
