#ifndef AHOCORASICK_H
#define AHOCORASICK_H

#include <vector>
#include <string>

#include "machine.h"

class AhoCorasick
{
    Machine machine;
    void addString(const std::string token);

public:
    AhoCorasick(const std::vector<std::string> &tokens);
    void matching(const std::string &str);
};

#endif // AHOCORASICK_H
