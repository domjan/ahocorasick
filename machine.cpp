#include "machine.h"

using namespace std;

/**
 * State machine constructor:
 * It creates one state
 */
Machine::Machine()
{
    initState = new State(false);
    initState->setErrorState(initState);
    currentState = initState;
}

/**
 * State machine destructor: deletes all states
 */
Machine::~Machine()
{
    delete initState;
}

/**
 * Tries to step with a character in the state machine.
 * We pass the string and iter separatelly for debugging purposes.
 * And it is usefull to know where we are: where the match is.
 */
void Machine::step(const string &str, int iter)
{
    currentState = currentState->step( str[iter] );
    if(currentState->isMatch())
    {
        cout<<"Match for the following words: ";

        vector<string> mw = currentState->matchWords();

        for(unsigned long i=0; i<mw.size(); i++)
        {
            std::string ss = mw[i];
            std::cout <<ss.c_str()<<" ";
        }
        cout <<" Position: "<<iter;
        cout<<endl;
    }
}
