#ifndef MACHINE_H
#define MACHINE_H

#include <state.h>

class Machine
{
    State *initState;
    State *currentState;

public:
    Machine();
    ~Machine();
    void step(const std::string &str, int iter);

    void setInitState()
    {
        currentState = initState;
    }

    /**
     * Adds a new state with a character
     *
     * @param character the new state character
     * @param match indicates that this is the end of the string (ie. matching state)
     * @param word we store the whole word to know what are we matching
     */
    void addNewState(char character, bool match, std::string word)
    {
        currentState = currentState->appendState(character, match, word);
    }

    /**
     * Sets all the error states and back edges in the automaton
     * Need to call for automaton initialization, after all the words added
     */
    void setErrorStates()
    {
        initState->setErrorStates();
    }
};

#endif // MACHINE_H
