#include "ahocorasick.h"

using namespace std;
#include <vector>

int main()
{
    //find these words in a long string:
    vector<string> vect;
    vect.push_back("hi");
    vect.push_back("hips");
    vect.push_back("hip");
    vect.push_back("hit");
    vect.push_back("chip");

    cout<<"Try to find the following words: "<<endl;
    for(unsigned long i = 0; i<vect.size(); i++) {
        cout << vect.at(i) << " ";
    }
    cout << endl;

    AhoCorasick aho(vect);

    string string1 = "microchips";
    string string2 = "lajdlajdhilaksjdljadchipmcvppewrnhiplskdchitldsjncwncwsl";

    cout<<"In text \""<<string1<<"\""<<endl;
    aho.matching(string1);

    cout<<endl<<"In text \""<<string2<<"\""<<endl;
    aho.matching(string2);
    return 0;
}
