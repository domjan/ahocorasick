#include "state.h"

using namespace std;

/**
 * State destructor: deletes its edges
 */
State::~State()
{
    map<char, State *>::iterator i = edges.begin();
    for(; i != edges.end(); i++)
    {
        delete i->second;
    }
}

/**
 * Try to step with a character
 *
 * @return If we cannot step with the character, then the function
 * recursively goes back to an error state and returns a state
 * (this can be an error or non-error state).
 * Otherwise it returns the next state.
 */
State *State::step(const char &character)
{
    std::map<char, State *>::iterator i = edges.find(character);
    if(i == edges.end())
    {
        if(error == this)
        {
            return this;
        }
        else
        {
            return error->step(character); //recursively go back
        }
    }
    else
    {
        return i->second;
    }
}


/**
 * Appends a new state if neccessary.
 * Sets the error state to the init state.
 */
State *State::appendState(const char &character, bool match, const string &word)
{
    std::map<char, State *>::iterator i = edges.find(character);
    if(i == edges.end())
    {
        State *state = new State(match);
        if(match)
        {
            state->appendWord(word);
        }
        state->setErrorState(error);
        edges[character] = state;
        return state;
    }
    else
    {
        State *state = i->second;
        if(match == true)
        {
            state->appendWord(word);
            state->setMatch(true);
        }
        return state;
    }
}


/**
 * Sets the error states, sets the depths, and the match states(if neccessary).
 * Basically, it does a breadth first search
 */
void State::setErrorStates()
{
    deque<State *> states;
    states.push_front(this);
    this->setDepth(0);
    while(states.size() > 0)
    {
        State *state = states.back();
        states.pop_back();
        map<char, State *> edgeMap = state->getEdges();
        map<char, State *>::iterator i = edgeMap.begin();
        map<char, State *>::iterator j = edgeMap.end();
        State *error_state = state->errorState();

        for(; i != j; i++)
        {
            char character = i->first;
            State *to = i->second;
            to->setDepth(state->getDepth()+1);
            if(state->depth > 0)
            {
                State *where_error = error_state->step(character); //where to step with the craracter from the error state?
                to->setErrorState(where_error);
                if(where_error->isMatch())
                {
                    to->appendWords(where_error->matchWords());
                    to->setMatch(true);
                }
            }
            states.push_front(i->second);
        }
    }
}
