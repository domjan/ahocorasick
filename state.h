#ifndef STATE_H
#define STATE_H

#include <map>
#include <iostream>
#include <deque>
#include <vector>

class State
{
    std::map<char, State*> edges;
    std::vector<std::string> matchWordList;
    State *error;
    bool match;
    int depth;

public:
    State(bool match) : match(match)
    { }

    ~State();

    void setErrorState(State *_error) { error = _error; }
    State *errorState() { return error; }

    void setMatch(bool _match) { match = _match; }
    bool isMatch() { return match; }

    void setDepth(int _depth) { depth = _depth; }
    int getDepth() { return depth; }

    void appendWord(const std::string &word)
    {
        matchWordList.push_back(word);
    }

    void appendWords(const std::vector<std::string> &words)
    {
        for(unsigned long i = 0; i<words.size(); i++)
        {
            matchWordList.push_back(words[i]);
        }
    }

    std::vector<std::string> matchWords()
    {
        return matchWordList;
    }

    std::map<char, State*> getEdges() { return edges; }
    State *step(const char &character);
    State *appendState(const char &character, bool match, const std::string &word);
    void setErrorStates();
};

#endif // STATE_H
